<?php
interface Class1 {
  public function GetMSG();
}

class Class2 implements Class1 {
  public function GetMSG() {
    echo "What your name?";
	echo "<br>";
	echo "............";
	echo "<br>";
  }
}

class Class3 implements Class1 {
  public function GetMSG() {
    echo "My Name is Luis A. Sierra";
	echo "<br>";
	echo "............";
	echo "<br>";
  }
}

class Class4 implements Class1 {
  public function GetMSG() {
    echo "So What irs your occupations?";
	echo "<br>";
	echo "............";
	echo "<br>";
  }
}

class Class5 implements Class1 {
  public function GetMSG() {
    echo "I'm a Computer System Engineer";
	echo "<br>";
	echo "............";
	echo "<br>";
  }
}

$cls2 = new Class2();
$cls3 = new Class3();
$cls4 = new Class4();
$cls5 = new Class5();

$cls2->GetMSG(); 
$cls3->GetMSG(); 
$cls4->GetMSG(); 
$cls5->GetMSG(); 
?>