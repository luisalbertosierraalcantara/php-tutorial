<?php
class Class1 {
  // Properties
  public $name;

  // Methods
  function set_name($name) {
    $this->name = $name;
  }
  function get_name() {
    return $this->name;
  }
}

$n = new Class1();
$n->set_name("Luis A. Sierra");

echo $n->get_name();

?>