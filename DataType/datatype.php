<?php
$name = 'Luis A. Sierra';                  //String
var_dump($name);
echo "<br><br>";

$age = 50;                                 //Integer
var_dump($age); 
echo "<br><br>";

$size = 6.7;                               //Float
var_dump($size); 
echo "<br><br>";

$status = true;                            //Boolean
var_dump($status); 
echo "<br><br>";

$colors = array("Blue","Red","Black");     //Array
var_dump($colors);
echo "<br><br>";

$none = null;                              //NULL
var_dump($none);
echo "<br><br>";

class Person {
  public $name;
  public function __construct($value) {
    $this->name = $value;
  }
  public function GetMSG() {
    return "My name is " . $this->name;;
  }
}
$d = new Person("Luis A. Sierra");          //Object
var_dump($d);

?>