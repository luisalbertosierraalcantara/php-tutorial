<html lang="en">
<body>
<form action="files2.php" method="POST" enctype="multipart/form-data">
   <input type="file" name="file_post">
   <input type ="submit" value="UPLOAD">
</form>
<br>
</body>
</html>

<?php
//************************************************
//CREATED By LUIS A. SIERRA
//************************************************
if (isset($_FILES["file_post"]["name"])){
	
		if ($_FILES["file_post"]["name"] != "")
		{
			
			$status = 1;
			$dir = "uploads/";
			$file = $dir . basename($_FILES["file_post"]["name"]);
			$file_type = strtolower(pathinfo($file,PATHINFO_EXTENSION));
			
			// Check if file already exists
			if (file_exists($file)) {
			  echo "Sorry, file already exists.<br>";
			  $status = 0;
			}
			
			// Check file size
			// 1 Megabyte = 1,000,000 bytes
			// 5 mb * 1,000,000 b = 5,000,000 byte
			if ($_FILES["file_post"]["size"] > 5000000) {  //5MB
			  echo "Sorry, your file is too large.<br>";
			  $status = 0;
			}
			
			// Allow certain file formats
			if($file_type != "jpg" && $file_type != "png" && $file_type != "jpeg"
			&& $file_type != "gif" ) {
			  echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.<br>";
			  $status = 0;
			}
			
			// Check if $status is set to 0 by an error
			if ($status == 0) {
			  echo "Sorry, your file was not uploaded.<br>";
			} 
			else 
			{
				if (move_uploaded_file($_FILES["file_post"]["tmp_name"], $file)) 
				{
				   echo "The file ". htmlspecialchars(basename( $_FILES["file_post"]["name"])). " has been uploaded.<br>";
				   echo "<b>Image View:</b> <br><br>";
				   echo '<img src="'.$file.'" width="100px" heght="100px" />';
				}
			}	
		}
}
?>