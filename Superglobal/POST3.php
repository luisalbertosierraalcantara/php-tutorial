<!DOCTYPE html>
<html>
<body>
<!-- CREATE TEXT EDIT IN HTML5 -->
<b>Name:</b><input type="text" name="name_post" id="name_id">

<!-- CREATE BUTTON IN HTML5 -->
<button onclick="sendPost()">SEND</button><br><br>
<div id="showdata"></div>

<script>
function sendPost() {
  //********************************************************	
  //CREATED By LUIS A. SIERRA
  //********************************************************
  //Intiate a HTTP request
  const xhttp = new XMLHttpRequest();
  //Set the HTTP method to POST
  xhttp.open("POST", "post3.php");
  //Set a valid request header
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  
  //Create a function to execute when the request is done
  xhttp.onload = function() {
	      //create empty html tag
          var temp = document.createElement('html');
		  //set the response to html tag
		  temp.innerHTML = xhttp.responseText;
		  //get p tag with the id:request
		  var request = temp.querySelector('#request');  
          //find div:showdata and insert b:tag with the result text  		  
		  document.getElementById("showdata").innerHTML = "<b>"+request.textContent+"</b>";
  }

  //get the value from input
  var input = document.getElementById("name_id");
  //Send the HTTP request with a variable name_post
  xhttp.send("name_post=" + input.value);
}
</script>

</body>
</html>

<?php
if (isset($_POST['name_post']))
{
   $name = $_POST['name_post'];
   echo "<p id='request'>You typed this: ".$name." </p>";
}
?>