<!DOCTYPE html>
<html>
<body>
<!-- CREATE TEXT EDIT IN HTML5 -->
<b>Name:</b><input type="text" id="name_id">

<!-- CREATE BUTTON IN HTML5 -->
<button onclick="sendGet()">SEND</button><br><br>
<div id="showdata"></div>

<script>
function sendGet() {
  //********************************************************	
  //CREATED By LUIS A. SIERRA
  //********************************************************
  //Intiate a HTTP request
  const xhttp = new XMLHttpRequest();
  //get the value from input
  var input = document.getElementById("name_id");
  //Set the HTTP method to GET
  xhttp.open("GET", "get3.php?name_get=" + input.value);
  //Set a valid request header
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  
  //Create a function to execute when the request is done
  xhttp.onload = function() {
	      //create empty html tag
          var temp = document.createElement('html');
		  //set the response to html tag
		  temp.innerHTML = xhttp.responseText;
		  //get p tag with the id:request
		  var request = temp.querySelector('#request');  
          //find div:showdata and insert b:tag with the result text  		  
		  document.getElementById("showdata").innerHTML = "<b>"+request.textContent+"</b>";
  }

  //Send the HTTP request with a variable name_get
  xhttp.send();
}
</script>

</body>
</html>

<?php
if (isset($_GET['name_get']))
{
   $name = $_GET['name_get'];
   echo "<p id='request'>You typed this: ".$name." </p>";
}
?>