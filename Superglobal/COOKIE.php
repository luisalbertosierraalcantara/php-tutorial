<?php

//86400 = 1 day
//The cookie will expire after 15 days (86400 * 15). 
//The "/" means that the cookie is available in entire website
$cookie_name = "user";
$cookie_value = "Luis A. Sierra";

//The setcookie() function must appear BEFORE the <html> tag.
setcookie($cookie_name, $cookie_value, time() + (86400 * 15), "/"); 
?>

<html>
<body>
<?php
if(isset($_COOKIE[$cookie_name])) {
  echo "Cookie: ".$cookie_name. "<br>";
  echo "Value: ".$_COOKIE[$cookie_name];
}else{
  echo "Cookie is empty";	
}
?>
</body>
</html>