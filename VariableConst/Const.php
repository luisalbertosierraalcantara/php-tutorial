<?php

//case-sensitive 
define("GREETING", "<p>Hello World!</p>"); 
echo GREETING;

//case-insensitive
define("GREETING", "<p>Hello World!</p>", true); 
echo greeting;

//by const keyword
const name = "<p>Luis A. Sierra</p>";
echo name;


?>