<?php
class Class1 {
  public $name;

  function __construct($name) {
    $this->name = $name;
  }
  
  function get_name() {
    return $this->name;
  }
}

$n = new Class1("Luis A. Sierra");
echo $n->get_name();

?>