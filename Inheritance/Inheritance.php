<?php

class Class1 {
  public $Account;

  public function setAccount($value) {
    $this->Account = $value;
  }
  
  public function getAccount() {
    return $this->Account;
  }
}

// Class2 is inherited from Class1
class Class2 extends Class1 {
   public function ShowData() {
     $this->setAccount(357886);
     echo "Account: ".$this->getAccount();
   }
}

$n = new Class2();
$n->ShowData();

?>