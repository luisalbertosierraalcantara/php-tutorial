<?php

function div($x,$y) {
	
    if ($x == 0 || $y == 0) {
        throw new Exception('Division by zero.');
    }
	
    return $y / $x;
}


try {
    echo div(0,5). "<br>";
	
} catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "<br>";
}

?>