<?php

echo "Convert to Time";
echo "<br>";
echo "-----------------------------------";
echo "<br>";
$text = "12:00pm";
$date =strtotime($text);

//It is not mandatory to cast, 
//because PHP does it automatically, 
//but it is good practice to do so,
echo "Time: ".(String) date("h:i:sa", $date);;
echo chr(13);

?>