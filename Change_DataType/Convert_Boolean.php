<?php

echo "Convert to Boolean";
echo "<br>";
echo "-----------------------------------";
echo "<br>";
$text= "True";
$Boleano = (bool) $text;

//It is not mandatory to cast, 
//because PHP does it automatically, 
//but it is good practice to do so,
echo "Boolean Boleano: ".(String) $Boleano;
echo chr(13);

?>