<?php

echo "Convert to Date";
echo "<br>";
echo "-----------------------------------";
echo "<br>";
$text = "13-10-2022";
$date =strtotime($text);

//It is not mandatory to cast, 
//because PHP does it automatically, 
//but it is good practice to do so,
echo "Date: ".(String) date("d/m/Y", $date);;
echo chr(13);

?>