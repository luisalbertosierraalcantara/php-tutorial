<?php

echo "Convert to DateTime";
echo "<br>";
echo "-----------------------------------";
echo "<br>";
$text = "13-10-2022 12:00pm";
$date =strtotime($text);

//It is not mandatory to cast, 
//because PHP does it automatically, 
//but it is good practice to do so,
echo "DateTime: ".(String) date("d/m/Y h:i:sa", $date);
echo chr(13);

?>