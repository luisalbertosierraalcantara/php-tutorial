<?php
echo "Convert to Integer";
echo "<br>";
echo "-----------------------------------";
echo "<br>";
$text = "15";
$number = (int) $text;

//It is not mandatory to cast, 
//because PHP does it automatically, 
//but it is good practice to do so,
echo "Int Number: ".(String) $number; 
echo chr(13);
?>