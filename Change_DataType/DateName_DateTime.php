<?php

echo "DayName of DateTime";
echo "<br>";
echo "-----------------------------------";
echo "<br>";
$DayName ="";
$text = "13-11-2022 12:00pm";
$date = strtotime($text);
$DayNumber = (int) date("w",$date); //0 (for Sunday) and 6 (for Saturday)

if ($DayNumber == 0) 
	$DayName = "Sunday";
else
if ($DayNumber == 1) 
	$DayName = "Monday";
else
if ($DayNumber == 2) 
	$DayName = "Tuesday";
else
if ($DayNumber == 3) 
	$DayName = "Wednesday";
else
if ($DayNumber == 4) 
	$DayName = "Thursday";
else
if ($DayNumber == 5) 
	$DayName = "Friday";
else
if ($DayNumber == 6) 
	$DayName = "Saturday"; 
			
echo "Day: ".$DayName;
echo chr(13);

?>