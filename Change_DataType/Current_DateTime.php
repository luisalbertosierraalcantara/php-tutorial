<?php

echo "Convert to Current DateTime";
echo "<br>";
echo "-----------------------------------";
echo "<br>";

date_default_timezone_set('America/New_York');
//It is not mandatory to cast, 
//because PHP does it automatically, 
//but it is good practice to do so,
echo "Current DateTime: ".(String) date("d/m/Y h:i:sa");
echo chr(13);

?>