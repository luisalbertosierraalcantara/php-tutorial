<?php

$var1 = 50;       // Integer
$var2 = 6.7;      // Float
$var3 = "hello";  // String
$var4 = true;     // Boolean
$var5 = NULL;     // NULL

//Converting to string by casting
$var1 = (string) $var1;
$var2 = (string) $var2;
$var3 = (string) $var3;
$var4 = (string) $var4;
$var5 = (string) $var5;

//verify the data type
var_dump($var1);
echo "<br>";
var_dump($var2);
echo "<br>";
var_dump($var3);
echo "<br>";
var_dump($var4);
echo "<br>";
var_dump($var5);

?>

