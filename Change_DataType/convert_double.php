<?php

echo "Convert to double";
echo "<br>";
echo "-----------------------------------";
echo "<br>";
$text = "19.5";;
$number = (float) $text;

//It is not mandatory to cast, 
//because PHP does it automatically, 
//but it is good practice to do so,
echo "Double Decimal: ".(String) $number;
echo chr(13);

?>