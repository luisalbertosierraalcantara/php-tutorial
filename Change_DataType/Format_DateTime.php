<?php

echo "Format DateTime";
echo "<br>";
echo "-----------------------------------";
echo "<br>";

$date = new DateTime();

//It is not mandatory to cast, 
//because PHP does it automatically, 
//but it is good practice to do so,
echo "Format DateTime: ".(String) $date->format('d F Y H:i:s');
echo chr(13);

?>