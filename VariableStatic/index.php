<?php

function getMsg() {
  static $name = "Luis A. Sierra"; // static scope
  echo "<p>Hello $name</p>";
}

getMsg();
getMsg();
getMsg();

?>