<?php
// Parent class
abstract class Class1 {
  public $name;
  
  public function __construct($name) {
    $this->name = $name;
  }
  
  abstract public function ShowData() : string;
}

// Child classes
class Class2 extends Class1 {
  public function ShowData() : string {
    return "$this->name";
  }
}

// Grandchild classes
class Class3 extends Class1 {
  public function ShowData() : string {
     return "$this->name";
  }
}


// Create objects from the child classes
$a = new Class2("Luis A. Sierra");
echo $a->ShowData();
echo "<br>";

$b = new Class3("Jhon Connor");
echo $b->ShowData();
echo "<br>";

?>